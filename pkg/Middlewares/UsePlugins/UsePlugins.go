package UsePlugins

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"kubehark/kubehark-framwork/pkg/Plugin/PluginFactory"
)

// Logger comment lint rebel
func UsePlugins() gin.HandlerFunc {
	return func(c *gin.Context) {
		err :=PluginFactory.RunBeforPlugin(c)
		if err!= nil{
			fmt.Println(err)
		}
		c.Next()
		PluginFactory.RunAfterPlugin(c)
	}
}
