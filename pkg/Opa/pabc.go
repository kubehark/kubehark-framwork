// Package pbac Policy Based Access Control
package opa

import (
	"context"
	"github.com/open-policy-agent/opa/rego"
)

// Input is struct for Opa input
type Input struct {
	Method    string `json:"method"`
	User      string `json:"user"`
	Labels    map[string]string `json:"label"`
	ClusterId string `json:"cluster_id"`
}

// Opa comment lint rebel
func Opa(labels map[string]string, method string, user string,
	modepath []string,
	RoleGrantsUrl string,
	UserRolesUrl string,
) (results rego.ResultSet, err error) {

	cont := context.TODO()
	var query rego.PreparedEvalQuery
	query, err = rego.New(
		rego.Query(`
allow = data.authz.allow
`),
		rego.Load(modepath, nil),
	).PrepareForEval(cont)

	//fmt.Println(err)
	if err != nil {
		return
	}

	input := map[string]interface{}{
		"method": method,
		"user":   user,
		"labels": labels,
		"userrolesurl":UserRolesUrl,
		"rolegrantsurl":RoleGrantsUrl,
	}



	results, err = query.Eval(cont, rego.EvalInput(input))


	//fmt.Println(modepath)
	//fmt.Printf("验证信息 %v  \n 验证结果 %v \n",input,results)

	if err != nil {
		return
	}

	return
}
