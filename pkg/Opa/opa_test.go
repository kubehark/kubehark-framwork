package opa

import (
	"fmt"
	"reflect"
	"testing"
)

func TestCmd(t *testing.T) {

	labels := map[string]string{
		"A":"A",
	}

	files := []string{
		"/Users/loganfang/go/gitee.com/kubehark/kubehark-deploy/mac/config/example.rego",
		"/Users/loganfang/go/gitee.com/kubehark/kubehark-deploy/mac/config/example_data.rego",
	}


	result ,err:= Opa(labels,
		"GET",
		"admin",
		files,
		"http://127.0.0.1:8081/v1/rolegrant/getlist",
		"http://127.0.0.1:8081/v1/userroles/getlist",
	)
	// [/Users/loganfang/go/gitee.com/kubehark/kubehark-deploy/mac/config/example.rego
	///Users/loganfang/go/gitee.com/kubehark/kubehark-deploy/mac/config/example_data.rego]
	// 验证信息 map[labels:map[A:A] method:GET rolegrantsurl:http://127.0.0.1:8081/v1/rolegrant/getlist user:admin userrolesurl:http://127.0.0.1:8081/v1/userroles/getlist]
	// 验证结果 [{[true] map[allow:false]}]
	fmt.Println(err)
	fmt.Println(result)
	fmt.Println(reflect.TypeOf(result))
	fmt.Println(1)
	//allow, ok := result[0].Bindings["allow"].(bool)
	//fmt.Println(allow,ok)
}
