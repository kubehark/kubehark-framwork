package oidc

import (
	"context"
	"fmt"
	"testing"
)

// 获取idtoken
func TestGetIdtoken(t *testing.T)  {
	authorizationToken := "eyJhbGciOiJSUzI1NiIsImtpZCI6IjZjNDQxMDM1OWYyNzE5OGNiYzdjNWU2MTNjMmVhY2IzOGFkYWYxNDkifQ.eyJpc3MiOiJodHRwOi8vMTA2LjEyLjU5LjEwOjU1NTYvZGV4Iiwic3ViIjoiQ2lRd09HRTROamcwWWkxa1lqZzRMVFJpTnpNdE9UQmhPUzB6WTJReE5qWXhaalUwTmpZU0JXeHZZMkZzIiwiYXVkIjoiZXhhbXBsZS1hcHAiLCJleHAiOjE2NDI4MzQyNzMsImlhdCI6MTY0Mjc0Nzg3MywiYXRfaGFzaCI6ImN5bk5UUW5HT0EtbDNqb25iSnV5YUEiLCJjX2hhc2giOiIzaW5JekJZSjVrZkFHcHNRQXBPWFZ3IiwiZW1haWwiOiJhZG1pbkBleGFtcGxlLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiYWRtaW4ifQ.QklG3MD1wS_DGBdHzoX7P9lXw9ouM7rfrLK-ViaiGmuR2wvQXSMgmao3dQ_tyZQhk3DiDbcgsIDzMM5lJ7bhB11HRrMudX_4zmkNhVH1-GW57GwjKLbRDCjwogetyBXal7vM328okNrrbR_QqM0MY5ngaXOYcFv2MCn4AyonbSe6j1c39B4KVWHn4IUMgZdeQH3r7ybISs7MG-a9I5tq1zo-FYTHr23nprhvArfRp3gV9BA1Lv12m_VD6vH7hZjWrD46jsw04Yo1Zq_KJPhOwv-Uh7LItgfMAa3JtOW9gvaytFW7QICWjH-M9OjKX2LnZ_75jL6eyliDLJGHPncLsQ"
	oidcApp, err := Setup(
		"example-app",
		"http://106.12.59.10:5556/dex",
		"ZXhhbXBsZS1hcHAtc2VjcmV0",
	)
	if err!= nil{
		fmt.Println(err.Error())
	}
	idToken, err := oidcApp.GetVerifier().Verify(context.Background(), authorizationToken)

	var claims Claims
	if err := idToken.Claims(&claims); err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(claims.Name)
}
