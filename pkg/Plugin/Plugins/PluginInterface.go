package Plugins

import "github.com/gin-gonic/gin"

type PluginInterface interface {
	SetConfigFromInterface(interface{}) PluginInterface
	RunPlugin(*gin.Context) error
}

