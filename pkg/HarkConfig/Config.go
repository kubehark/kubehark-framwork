package HarkConfig

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type HarkConfig struct {
	RunningConfig RunningConfig `json:"running_config"`
	PluginConfig PluginConfig `json:"plugin_config"`
}

type RunningConfig struct {
	Port   string `json:"port"`
	Ip     string `json:"ip"`
	SSLkey  string `json:"sslkey"`
	SSLKSecret string `json:"sslse"`
	K8sConfig string `json:"k8sconfig"`
}

type PluginConfig struct {
	UseBefoPlugins  []string               `json:"use_befo_plugins""`
	UseAfterPlugins []string               `json:"use_after_plugins"`
	PluginsConfig   map[string]interface{} `json:"plugins_config"`
}

func NewConfigFromPath(path string) (config *HarkConfig,err error)  {
	f,err := os.Open(path)
	if err!= nil{
		return nil, err
	}
    bytes,err:=ioutil.ReadAll(f)
    if err!= nil{
    	return nil, err
	}

    config =&HarkConfig{}
	json.Unmarshal(bytes,config)
    return config,nil
}