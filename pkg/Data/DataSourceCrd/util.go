package DataSourceCrd

import (
	"errors"
	"fmt"
)

const (
	RoleGrant = "RoleGrant"
	Credential = "Credential"
	UserRoles = "UserRoles"
	NameSpaces = "default"
)

// 从inform中获取数据
func GetDataFromInformer(name string,key string)(interface{},bool,error)  {
	fmt.Println(NameSpaces+"/"+key)
	switch name {
	case RoleGrant:
		return RoleGrantInformer.GetStore().GetByKey(NameSpaces+"/"+key)
	case Credential:
		return CredentialInformer.GetStore().GetByKey(NameSpaces+"/"+key)
	case UserRoles:
		return UserRolesInformer.GetStore().GetByKey(NameSpaces+"/"+key)
	default:
		return nil,false,errors.New("该informer不存在")
	}
}
