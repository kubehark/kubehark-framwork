package DataSourceCrd

import (
	"fmt"
	"gitee/kubehark/kubehark-operator/client/clientset/versioned"
	v1 "gitee/kubehark/kubehark-operator/client/informers/externalversions/kubehark/v1"
	"k8s.io/client-go/tools/clientcmd"
	"kubehark/kubehark-framwork/pkg/Data/DataSourceCrd/model"
	"kubehark/kubehark-framwork/pkg/Data/DataSourceCrd/service"
	"testing"
	"time"
)

var kubeconfigstring = `
apiVersion: v1
clusters:
- cluster:
    insecure-skip-tls-verify: true
    server: https://106.12.59.10:6443
  name: hark-1
contexts:
- context:
    cluster: hark-1
    user: admin
  name: master
current-context: master
kind: Config
preferences: {}
users:
- name: admin
  user:
    username: admin
    password: admin
`

// TestCmd comment lint rebel
func TestCmd(t *testing.T) {
	Init("/Users/loganfang/go/src/gitee/kubehark/kubehark-framwork/k8sconfig_dev")
	Credential_service := service.Credential_service{
		Client: ClientCrd,
	}
	cd := model.Credential_model{
		Name:       "cred",
		Kubeconfig: kubeconfigstring,
		Labels: map[string]string{
			"A": "A",
			"B": "B",
			"c": "c2",
		},
	}
	fmt.Println(Credential_service.Post(cd))
	fmt.Println("执行成功")

	//fmt.Println(Credential_service.Delete(cd))
}


func TestGet(t *testing.T) {
	Init("/Users/loganfang/go/src/gitee/kubehark/kubehark-framwork/example/k8sconfig_dev")
	Credential_service := service.Credential_service{
		Client: ClientCrd,
	}
	lists,_ := Credential_service.GetList(model.Credential_model{})

	fmt.Println(lists[0].Name)
}

func TestInformer(t *testing.T)  {
	Init("/Users/loganfang/go/gitee.com/kubehark/kubehark-framwork/example/k8sconfig_dev")
	time.Sleep(time.Second*3)
	fmt.Println(GetDataFromInformer(Credential, "default/local"))
	fmt.Println(RoleGrantInformer.GetStore().List())
}

func TestInformers(t *testing.T)  {
	serverKubeConfig := "/Users/loganfang/go/src/gitee/kubehark/kubehark-framwork/example/k8sconfig_dev"
	config, err := clientcmd.BuildConfigFromFlags("", serverKubeConfig)
	if err != nil {
		return
	}
	ClientCrd = versioned.NewForConfigOrDie(config)

	CredentialInformers:=v1.NewCredentialInformer(ClientCrd,
		"",
		10*time.Second,
		nil)

	var stopCh <-chan struct{}
	go CredentialInformers.Run(nil)
	for {
		fmt.Println(CredentialInformers.GetStore().List())
		fmt.Println(CredentialInformers.GetStore().ListKeys())
		fmt.Println(CredentialInformers.GetStore().GetByKey("default/baidu-cloud-fyl"))
		time.Sleep(time.Second)
	}
	fmt.Println(11)
	<-stopCh

}

