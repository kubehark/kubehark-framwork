package service

import (
	"context"
	"errors"
	v1 "gitee/kubehark/kubehark-operator/api/kubehark/v1"
	"gitee/kubehark/kubehark-operator/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"kubehark/kubehark-framwork/pkg/Data/DataSourceCrd/model"
)

type Rolegrant_service struct {
	Client *versioned.Clientset
}



func (this *Rolegrant_service) Post(model model.Rolegrant_model) error {
	if model.Name == "" {
		return errors.New("name 为必填项目")
	}
	CreateModel := v1.RoleGrant{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.GroupVersion.Group,
			Kind:       v1.GroupVersion.Group,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:   model.Name,
			Namespace: "default",
		},
		Spec: v1.RoleGrantSpec{
			Method: model.Method,
			Labels: model.Labels,
		},
	}
	_, err := this.Client.KubeharkV1().RoleGrants("default").Create(context.TODO(), &CreateModel, metav1.CreateOptions{})
	return err
}

func (this *Rolegrant_service)Delete(model model.Rolegrant_model)error  {
	if model.Name == "" {
		return errors.New("name 为必填项目")
	}
	return  this.Client.KubeharkV1().RoleGrants("default").Delete(context.TODO(), model.Name, metav1.DeleteOptions{})
}


func (this *Rolegrant_service)Put(model model.Rolegrant_model)error  {
	if model.Name == "" {
		return errors.New("name 为必填项目")
	}


	data,err:=this.Client.KubeharkV1().RoleGrants("default").Get(context.TODO(), model.Name,metav1.GetOptions{})
	if err!=nil{
		return  err
	}



	CreateModel := v1.RoleGrant{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.GroupVersion.Group,
			Kind:       v1.GroupVersion.Group,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:   model.Name,
			Namespace: "default",
			ResourceVersion: data.ObjectMeta.ResourceVersion,
		},
		Spec: v1.RoleGrantSpec{
			Method: model.Method,
			Labels: model.Labels,
		},
	}
	_, err = this.Client.KubeharkV1().RoleGrants("default").Update(context.TODO(), &CreateModel, metav1.UpdateOptions{})
	return err
}



func (this *Rolegrant_service)Get(model model.Rolegrant_model)  (*model.Rolegrant_model ,error)  {
	if model.Name == "" {
		return nil, errors.New("name 为必填项目")
	}
	data,err:=this.Client.KubeharkV1().RoleGrants("default").Get(context.TODO(), model.Name,metav1.GetOptions{})
	if err!=nil{
		return nil, err
	}

	model.Name = data.ObjectMeta.Name
	model.Method = data.Spec.Method
	model.Labels = data.Spec.Labels
	return  &model,nil
}



func (this *Rolegrant_service)GetList(mo model.Rolegrant_model)  ([]*model.Rolegrant_model ,error)  {

	data,err:=this.Client.KubeharkV1().RoleGrants("default").List(context.TODO(),metav1.ListOptions{})
	if err!=nil{
		return nil, err
	}


	returndata := []*model.Rolegrant_model{}

	for _,v := range data.Items{
		da := model.Rolegrant_model{}
		da.Name = v.Name
		da.Labels = v.Spec.Labels
		da.Method = v.Spec.Method
		returndata = append(returndata, &da)
	}
	return  returndata,nil
}