package service

import (
	"context"
	"errors"
	"kubehark/kubehark-framwork/pkg/Data/DataSourceCrd/model"
	"gitee/kubehark/kubehark-operator/api/kubehark/v1"
	"gitee/kubehark/kubehark-operator/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Credential_service struct {
	Client *versioned.Clientset
}

//Post() *Request
//Put() *Request
//Patch(pt types.PatchType) *Request
//Get() *Request
//Delete() *Request

func (this *Credential_service) Post(model model.Credential_model) error {
	if model.Name == "" {
		return errors.New("name 为必填项目")
	}
	CreateModel := v1.Credential{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.GroupVersion.Group,
			Kind:       v1.GroupVersion.Group,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:   model.Name,
			Labels: model.Labels,
			Namespace: "default",
		},
		Spec: v1.CredentialSpec{
			ClusterName: model.Name,
			Kubeconfig:  model.Kubeconfig,
		},
	}
	_, err := this.Client.KubeharkV1().Credentials("default").Create(context.TODO(), &CreateModel, metav1.CreateOptions{})
	return err
}

func (this *Credential_service)Delete(model model.Credential_model)error  {
	if model.Name == "" {
		return errors.New("name 为必填项目")
	}
	return  this.Client.KubeharkV1().Credentials("default").Delete(context.TODO(), model.Name, metav1.DeleteOptions{})
}


func (this *Credential_service)Put(model model.Credential_model)error  {
	if model.Name == "" {
		return errors.New("name 为必填项目")
	}


	data,err:=this.Client.KubeharkV1().Credentials("default").Get(context.TODO(), model.Name,metav1.GetOptions{})
	if err!=nil{
		return  err
	}



	CreateModel := v1.Credential{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.GroupVersion.Group,
			Kind:       v1.GroupVersion.Group,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:   model.Name,
			Labels: model.Labels,
			Namespace: "default",
			ResourceVersion: data.ObjectMeta.ResourceVersion,
		},
		Spec: v1.CredentialSpec{
			ClusterName: model.Name,
			Kubeconfig:  model.Kubeconfig,
		},
	}
	_, err = this.Client.KubeharkV1().Credentials("default").Update(context.TODO(), &CreateModel, metav1.UpdateOptions{})
	return err
}



func (this *Credential_service)Get(model model.Credential_model)  (*model.Credential_model ,error)  {
	if model.Name == "" {
		return nil, errors.New("name 为必填项目")
	}
	data,err:=this.Client.KubeharkV1().Credentials("default").Get(context.TODO(), model.Name,metav1.GetOptions{})
	if err!=nil{
		return nil, err
	}

	model.Name = data.Spec.ClusterName
	model.Kubeconfig = data.Spec.Kubeconfig
	model.Labels = data.ObjectMeta.Labels
	return  &model,nil
}

func (this *Credential_service)GetList(mo model.Credential_model)  ([]*model.Credential_model ,error)  {

	data,err:=this.Client.KubeharkV1().Credentials("default").List(context.TODO(),metav1.ListOptions{})
	if err!=nil{
		return nil, err
	}


	returndata := []*model.Credential_model{}

	for _,v := range data.Items{
		da := model.Credential_model{}
		da.Kubeconfig = v.Spec.Kubeconfig
		da.Labels = v.Labels
		da.Name = v.Name
		returndata = append(returndata, &da)
	}
	return  returndata,nil
}
