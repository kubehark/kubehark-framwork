package DataSourceCrd

import (
	"gitee/kubehark/kubehark-operator/client/clientset/versioned"
	v1 "gitee/kubehark/kubehark-operator/client/informers/externalversions/kubehark/v1"
	"k8s.io/client-go/tools/cache"
	"kubehark/kubehark-framwork/pkg/Data/DataSourceCrd/service"
	"time"

	"k8s.io/client-go/tools/clientcmd"
)

var ClientCrd *versioned.Clientset
var CredentialInformer cache.SharedIndexInformer
var RoleGrantInformer cache.SharedIndexInformer
var UserRolesInformer cache.SharedIndexInformer

var Credential_service service.Credential_service
var Rolegrant_service service.Rolegrant_service
var Userroles_service service.Userroles_service

func Init(serverKubeConfig string) {
	config, err := clientcmd.BuildConfigFromFlags("", serverKubeConfig)
	if err != nil {
		return
	}
	ClientCrd = versioned.NewForConfigOrDie(config)
	InitServer()
	InitInform()
}

// 初始化inform 模块
func InitInform() {
	CredentialInformer = v1.NewCredentialInformer(
		ClientCrd,
		NameSpaces,
		10*time.Second,
		nil,
	)



	RoleGrantInformer = v1.NewRoleGrantInformer(
		ClientCrd,
		NameSpaces,
		10*time.Second,
		nil,
	)

	UserRolesInformer = v1.NewUserRolInformer(
		ClientCrd,
		NameSpaces,
		10*time.Second,
		nil,
	)

	go CredentialInformer.Run(nil)
	go RoleGrantInformer.Run(nil)
	go UserRolesInformer.Run(nil)
}

// 初始化server模块
func InitServer() {
	// 创建Credential的server
	Credential_service = service.Credential_service{
		Client: ClientCrd,
	}
	// 创建Rolegrant的server
	Rolegrant_service = service.Rolegrant_service{
		Client: ClientCrd,
	}
	// 创建Userroles的server
	Userroles_service = service.Userroles_service{
		Client: ClientCrd,
	}
}
