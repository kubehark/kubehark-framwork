package model

type Credential_model struct {
	Name       string `json:"name"`
	Kubeconfig string `json:"kubeconfig"`
	Labels map[string]string `json:"labels"`
}


