package model

type Userroles_model struct {
	//Name      string   `json:"name"`
	UserName  string   `json:"user_name"`
	Roles     []string `json:"roles"`
	WhiteList []string `json:"white_list"`
}
