IMG ?= fyl253711/kubehark-framework:v1
build-example:
	go build -o ./example/manager .
run-example: build-example
	./example/manager server --configpath /Users/loganfang/go/gitee.com/kubehark/kubehark-deploy/mac/config/config_example.json
build-linux:  ## Build manager binary.
	go mod tidy && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o example/manager .
docker-build: build-linux  ## Build docker image with the manager.
	docker build -t ${IMG} .
docker-push: docker-build ## Build docker image with the manager.
	docker push  ${IMG}