FROM nginx
WORKDIR /
COPY   ./example  /
USER 8081:8081
ENTRYPOINT ["/manager"]