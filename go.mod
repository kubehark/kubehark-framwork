module kubehark/kubehark-framwork

go 1.16

require (
	gitee/kubehark/kubehark-operator v0.0.0-00010101000000-000000000000
	github.com/coreos/go-oidc/v3 v3.1.0
	github.com/gin-gonic/gin v1.7.7
	github.com/open-policy-agent/opa v0.36.1
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	k8s.io/apimachinery v0.23.2
	k8s.io/client-go v0.23.2
	k8s.io/kubectl v0.22.4
)

require (
	github.com/mitchellh/mapstructure v1.4.3
	github.com/spf13/cobra v1.3.0
	k8s.io/cli-runtime v0.23.2 // indirect
	k8s.io/component-base v0.23.2 // indirect
)

replace gitee/kubehark/kubehark-operator => ../kubehark-operator
